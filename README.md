# Helmholtz Cloud Services

Statistics on number of Helmholtz Cloud Services.

* Canonical list: <https://plony.helmholtz.cloud/sc/public-service-portfolio>
* [Issue tracker for changes and maintenance](https://codebase.helmholtz.cloud/hifis/cloud/service-portfolio-management/maintenance-of-service-information/-/issues/?sort=created_date&state=all&first_page_size=200)
